# This applies a markup to labor costs by extracting it from the workpiece.
# You can set your labor markup with a default value, and modify it at quote time.

markup = var('Markup Percentage', 0, 'Percent markup on labor cost', number)
labor_cost = get_workpiece_value('labor_cost', 0)

PRICE = labor_cost * markup / 100
DAYS = 0
