# This swiss mill operation is an extension of the work center operation template, but contextualizes it
# for swiss mill based processes. To estimate a runtime, it uses a base material removal rate and applies
# it to a cylindrical stock piece extracted from the Paperless Parts lathe interrogation. You can set
# this material removal rate and adjust it at quote time.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
lathe = analyze_lathe()

turning_radius = var('Turning Radius', 0, 'Outermost radius of part in inches', number, frozen=False)
turning_radius.update(lathe.stock_radius)
turning_radius.freeze()

turning_length = var('Turning Length', 0, 'Length of part along turning axis in inches', number, frozen=False)
turning_length.update(lathe.stock_length)
turning_length.freeze()

radial_buffer = var('Radial Stock Buffer, in', 0.0625, 'Buffer applied to the outer radius of part in inches', number)
length_buffer = var('Length Stock Buffer, in', 0.125, 'Buffer to the length of the part along the turning axis in inches', number)

stock_radius = turning_radius + radial_buffer
stock_length = turning_length + length_buffer

vol_cut_rate = var('Volume Cut Rate', 70, 'Cu. In./hr', number, frozen=False)
stock_volume = stock_radius**2 * 3.1415926535 * turning_length
volume_removal = stock_volume - part.volume

# if there is a large amount of volume removal, we will increase the cut rate
if volume_removal > 15:
    vol_cut_rate.update(84)
vol_cut_rate.freeze()

# minimum 30 seconds of runtime
runtime = var('runtime', 0, 'Runtime, specified in hours', number, frozen=False)
est_runtime = stock_volume / vol_cut_rate
est_runtime = max(0.25 / 60, est_runtime)  # 15 second floor
est_runtime = min(10 / 60, est_runtime)  # 10 minute ceiling
runtime.update(est_runtime)
runtime.freeze()

# assume one hour setup time
setup_time = var('setup_time', 1, 'Setup time, specified in hours', number)

setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
crew = var('Crew', 1, 'Number of people assigned to attend work center', number)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 100, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 0, 'Percentage of time work center must be attended', number)

total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0
