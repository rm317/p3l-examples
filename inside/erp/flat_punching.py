# This flat punching operation is an extension of the work center operation template, but contextualizes it
# for flat punching operations. Simple contours (circles, squares, rectangles, slots, obrounds) below your custom
# max size threshold will be assumed to cut with a single hit from a punch tool. All other contours are assumed
# to be multi-hit features. We provide you the multi-hit cut length to estimate pricing of these operations.

units_in()
sheet_metal = analyze_sheet_metal(is_punch=True, is_laser=False)

single_hit_setups = var('Single Hit Setups', 0, 'Number of tools to make single-hit contours', number, frozen=False)
single_hit_count = var('Single Hit Count', 0, 'Total number of single hit strokes', number, frozen=False)
multi_hit_cut_length = var('Multi Hit Length', 0, 'mm', number, frozen=False)

single_hit_setups.update(sheet_metal.punch_single_hit_setups)
single_hit_setups.freeze()
single_hit_count.update(sheet_metal.punch_single_hit_count)
single_hit_count.freeze()
multi_hit_cut_length.update(sheet_metal.punch_multi_hit_cut_length)
multi_hit_cut_length.freeze()

setup_time = var('setup_time', 0, 'Setup Time in hours', number, frozen=False)
runtime = var('runtime', 0, 'Runtime per part in hours', number, frozen=False)

base_setup_time = var('Base Setup Time', 30.0, 'Base setup time in minutes', number)
setup_time_per_tool = var('Setup time per tool', 10.0, 'Setup time per unique punch tool in minutes', number)
setup_time.update(base_setup_time / 60.0 + setup_time_per_tool * single_hit_setups / 60.0)

# apply a runtime per punch and estimate number of punches to accomplish multi hit features using a factor to runtime
runtime_per_punch = var('Time per punch', 1.5, 'Time per punch in seconds', number)
multi_hit_per_punch_length = var(
    'Multi hit per punch length', 2, 'Amount of cut length removed per stroke for multi-hit contours', number
)
multi_hit_count = var('Multi hit count', 0, 'Total number of hits to punch multi-hit contours', number, frozen=False)
multi_hit_count.update(ceil(multi_hit_cut_length / multi_hit_per_punch_length))
multi_hit_count.freeze()

# update runtime to sheet metal cutting perimeter
runtime.update((single_hit_count + multi_hit_count) * runtime_per_punch / 3600.0)
runtime.freeze()

# now apply standard work center logic
setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
crew = var('Crew', 1, 'Number of people assigned to attend work center', number)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 100, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 0, 'Percentage of time work center must be attended', number)

total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0
