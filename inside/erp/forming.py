# Extends the work center operation for sheet metal forming.
# Uses Paperless Parts sheet metal interrogation to pull out bends from a geometry
# and applies a runtime for each bending operation found.
# Applies logic that if the sheet of metal is larger than a certain threshold in X or Y,
# the crew will be increased from 1 to 2.

units_in()
sheet_metal = analyze_sheet_metal()

# setup: 15 min / bend
setup_time_per_bend = var('Setup Time Per Bend (hr)', 0.25, 'hr', number)
# runtime: 15 seconds / bend
run_time_per_bend = var('Run Time Per Bend (seconds)', 15, 'seconds per bend', number)

bend_count = var('Bend Count', 0, 'total bends', number, frozen=False)
bend_count.update(sheet_metal.bend_count)
bend_count.freeze()

setup_time = var('setup_time', 0, 'Setup time, specified in hours', number, frozen=False)
setup_time.update(bend_count * setup_time_per_bend)
setup_time.freeze()

runtime = var('runtime', 0, 'Runtime, specified in hours', number, frozen=False)
runtime.update(bend_count * run_time_per_bend / 3600)
runtime.freeze()

setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 80, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 100, 'Percentage of time work center must be attended', number)

crew_size_threshold = var('Crew Size Threshold', 48, 'Size threshold in inches to increase crew to 2', number)
crew = var('Crew', 1, 'Number of people assigned to complete forming operations', number, frozen=False)
if sheet_metal.size_x > crew_size_threshold or sheet_metal.size_y > crew_size_threshold:
    crew.update(2)
crew.freeze()

total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0