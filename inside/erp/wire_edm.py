# This wire edm operation is an extension of the work center operation template, but contextualizes it
# for wire edm processes.
# Establishes a ceiling for wire edm cut rates extracted from cutting steel parts at varying cut depths.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
wire = analyze_wire_edm()

min_cut_rate = 0.007
cut_rate_slope = 0 - 0.009
cut_rate_intercept = 0.05

cut_rate_calculated = cut_rate_slope * wire.average_cut_depth + cut_rate_intercept
if cut_rate_calculated < min_cut_rate:
    cut_rate_use = min_cut_rate
else:
    cut_rate_use = cut_rate_calculated

cut_rate = var('Cut Rate', 0, 'in/min', number, frozen=False)
cut_rate.update(cut_rate_use)
cut_rate.freeze()
cut_length = var('Cut Length, in', 0, 'in', number, frozen=False)

setup_time_per_setup = var('Base Setup Time per setup', 60.0, 'minutes', number)
setup_time = var('setup_time', 0, 'Total setup time, specified in hours', number, frozen=False)
setup_time.update(wire.setup_count * setup_time_per_setup / 60.0)
setup_time.freeze()

runtime_per_drilled_hole = var('Runtime Per Drilled Hole', 2.5, 'minutes', number)
total_drilled_runtime = wire.pierce_count * runtime_per_drilled_hole / 60.0

cut_length.update(wire.cut_length)
cut_length.freeze()

runtime = var('runtime', 0, 'Runtime per part, specified in hours', number, frozen=False)
runtime.update(total_drilled_runtime + cut_length / cut_rate / 60.0)
runtime.freeze()

setup_labor_rate = var('Setup Labor Rate', 0, 'Labor cost per hour to setup work center', currency)
run_labor_rate = var('Run Labor Rate', 0, 'Labor cost per hour to attend work center machine', currency)
crew = var('Crew', 1, 'Number of people assigned to attend work center', number)
machine_rate = var('Machine Rate', 0, 'Cost per hour to keep work center machine occupied', currency)
efficiency = var('Efficiency', 100, 'Percentage of real time the work center is running with regards to runtime', number)
percent_attended = var('Percent Attended', 0, 'Percentage of time work center must be attended', number)

total_cycle_time = part.qty * runtime
total_machine_occupied_time = total_cycle_time / efficiency * 100
total_attended_time = total_machine_occupied_time * percent_attended / 100

setup_cost = setup_time * setup_labor_rate
work_center_usage_cost = total_machine_occupied_time * machine_rate
run_labor_cost = total_attended_time * run_labor_rate * crew

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost + run_labor_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + work_center_usage_cost)

PRICE = setup_cost + work_center_usage_cost + run_labor_cost
DAYS = 0