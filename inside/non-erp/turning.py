# This turning operation is an extension of the work center operation template, but contextualizes it
# for turning based processes. To estimate a runtime, it uses a base material removal rate and applies
# it to a cylindrical stock piece extracted from the Paperless Parts lathe interrogation. You can set
# this material removal rate and adjust it at quote time.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
lathe = analyze_lathe()

turning_radius = var('Turning Radius', 0, 'Outermost radius of part in inches', number, frozen=False)
turning_radius.update(lathe.stock_radius)
turning_radius.freeze()

turning_length = var('Turning Length', 0, 'Length of part along turning axis in inches', number, frozen=False)
turning_length.update(lathe.stock_length)
turning_length.freeze()

radial_buffer = var('Radial Stock Buffer, in', 0.0625, 'Buffer applied to the outer radius of part in inches', number)
length_buffer = var('Length Stock Buffer, in', 0.125, 'Buffer to the length of the part along the turning axis in inches', number)

stock_radius = turning_radius + radial_buffer
stock_length = turning_length + length_buffer

vol_cut_rate = var('Volume Cut Rate', 70, 'Cu. In./hr', number, frozen=False)
stock_volume = stock_radius**2 * 3.1415926535 * turning_length
volume_removal = stock_volume - part.volume

# if there is a large amount of volume removal, we will increase the cut rate
if volume_removal > 15:
    vol_cut_rate.update(84)
vol_cut_rate.freeze()

runtime = var('runtime', 0, 'Runtime, specified in hours', number, frozen=False)
runtime.update(stock_volume / vol_cut_rate)
runtime.freeze()

# assume one hour setup time
setup_time = var('setup_time', 1, 'Setup time, specified in hours', number)

labor_rate = var('Labor Rate', 0, 'Cost per hour for setup', currency)
machine_rate = var('Machine Rate', 0, 'Cost per hour for run', currency)

total_cycle_time = part.qty * runtime

setup_cost = labor_rate * setup_time
machine_cost = machine_rate * total_cycle_time

# populate costs to workpiece so they can be referenced in later ops
set_workpiece_value('labor_cost', get_workpiece_value('labor_cost', 0) + setup_cost)
set_workpiece_value('machine_cost', get_workpiece_value('machine_cost', 0) + machine_cost)

PRICE = setup_cost + machine_cost
DAYS = 0
