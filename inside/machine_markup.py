# This applies a markup to machine costs by extracting it from the workpiece.
# You can set your machine markup with a default value, and modify it at quote time.

markup = var('Markup Percentage', 0, 'Percent markup on machine cost', number)
machine_cost = get_workpiece_value('machine_cost', 0)

PRICE = machine_cost * markup / 100
DAYS = 0
