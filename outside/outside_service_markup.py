# This applies a markup to outside service price by extracting it from the workpiece.
# You can set your outside service markup with a default value, and modify it at quote time.

markup = var('Markup Percentage', 0, 'Percent markup on outside service cost', number)
outside_service_cost = get_workpiece_value('outside_service_cost', 0)

PRICE = outside_service_cost * markup / 100
DAYS = 0
